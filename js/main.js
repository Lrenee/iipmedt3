//hendel buttons
const buttonBackwards = document.getElementById("green-box");
const buttonForward = document.getElementById("red-box");
const buttonNeutral = document.getElementById("blue-box");
const hendel = document.getElementById("hendel");

const triangleButtonForward = document.getElementById("triangleButtonForward");
const triangleButtonBackwards = document.getElementById("triangleButtonBackwards");
const circleButtonNeutral = document.getElementById("circleButtonNeutral");
const circleNoodButton = document.getElementById("circleNoodButton");

//noodknop
const button_top = document.getElementById("button_top");

//snelheid
const timerText = document.getElementById("timer");
var totalSeconds = 0;

const environmentMovement = document.getElementById("environment");
const cabine = document.getElementById("cabine");
const bordSnelheid = document.getElementById("bordSnelheid");
const rig = document.getElementById("rig");
const camera = document.getElementById("camera");
const emergency = document.getElementById("emergency");

//elementen voor uitleg
const vraagteken = document.getElementById("js--vraagtekenLevel1");
const vraagtekenTwee = document.getElementById("js--vraagtekenLevel2");
const vraagtekenDrie = document.getElementById("js--vraagtekenLevel3");
const vraagtekenVier = document.getElementById("js--vraagtekenLevel4");
const verderButton = document.getElementById("verder");
const uitlegEen = document.getElementById("uitleg1");
const uitlegTwee = document.getElementById("uitleg2");
const startButton = document.getElementById("start");
const uitlegLevelTwee = document.getElementById("level2");
const uitlegLevelTweeTwee = document.getElementById("level3");
const uitlegLevelDrie = document.getElementById("level4");
const uitlegLevelVier = document.getElementById("js--level5");
const verderButtonTwee = document.getElementById("verderTwee");
const verderButtonDrie = document.getElementById("verderDrie");
const verderButtonVier = document.getElementById("verderVier");
const verderButtonVijf = document.getElementById("verderVijf");
const verderButtonZes = document.getElementById("js--verderZes");
const eindeUitleg = document.getElementById("eindeLevelVier");
const uitlegSBord= document.getElementById("js--uitlegSbord");
const verderSBord = document.getElementById("verderSBord");

const opdracht = document.getElementById("opdrachtText");
const uitlegKnoppen = document.getElementById("uitlegKnoppen");

//obstakel
const obstalkelKoe = document.getElementById("obstakel");

//result obstakel
const succesObstakel = document.getElementById("succes");
const failedObstakel = document.getElementById("failed");

//S-bord
const Sbord = document.getElementById("bordS");

//verschillende scenes
const scene1 = document.getElementById("scene1");
const scene2 = document.getElementById("scene2");

//level 4

const environmentMovement2 = document.getElementById("js--environment2");
const eenSter = document.getElementById("js--1ster");
const tweeSter = document.getElementById("js--2ster");
const drieSter = document.getElementById("js--3ster");
const helaas = document.getElementById("js--helaas");
const verderLaatste = document.getElementById("js--verderLaatste");

//geluid
const geluidStart = new Audio("mp3/omroepenVertrek.mp3");
const geluidFluit = new Audio("mp3/fluitGeluid.mp3");
geluidFluit.volume = 0.05;
const geluidAankomst = new Audio("mp3/omroepenAankomst.mp3");
const ingesproken1 = new Audio("mp3/ingesproken1.mp3");
const ingesproken2 = new Audio("mp3/ingesproken2.mp3");
const ingesproken3 = new Audio("mp3/ingesproken3.mp3");
const ingesproken4 = new Audio("mp3/ingesproken4.mp3");
const ingesproken5 = new Audio("mp3/ingesproken5.mp3");
const ingesproken6 = new Audio("mp3/ingesproken6.mp3");
const uitleg1 = new Audio("mp3/uitleg1.mp3");
const uitleg2 = new Audio("mp3/uitleg2.mp3");
const uitleg3 = new Audio("mp3/uitleg3.mp3");
const uitleg4 = new Audio("mp3/uitleg4.mp3");
const uitleg5 = new Audio("mp3/uitleg5.mp3");

//variabelen voor controleren van de status van de trein
let isRiding = false;
let isStopping = false;
let isAlreadyRiding = false;
let timerSet = false;
let checkSecond = false;
let setSnelheid;
let checkStart;
let increaseSpeed;
let obstakelTouched;
let noodButton;
let cowStart = false;
let opdrachtVoltooid = false;
let buttonFlickering = false;
let buttonForwardTouched = false;
let buttonNeutralTouched = false;
let buttonBackwardsTouched = false;
let buttonNoodTouched = false;
let intervalFlickering;


//uitleg verschillende knoppen
let tekstUitlegEen = " Welkom in de treincabine. Je gaat hier leren om een trein te besturen. Je ziet onwijs veel knoppen om je heen en daarom moet je natuurlijk weten wat elke knop doet.";
let tekstUitlegTwee = " Om de trein te besturen, maak je gebruik van de hendel aan de rechterkant. Door de hendel naar voren te bewegen, breng je de trein in beweging. Breng nu zelf de trein in beweging!";
let tekstUitlegDrie = " Goed gedaan! Wanneer je de aangegeven snelheid bereikt hebt, moet de trein zijn snelheid behouden. Hiervoor gebruik je de neutraal stand in de trein. Door de hendel rechtop de zetten, blijft de trein dezelfde snelheid rijden. Raak nu de neutraal knop aan!";
let tekstUitlegVier = " Bij het bereiken van het eindperron, moet je de trein tot stilstand brengen. Door de hendel naar achteren te bewegen, komt de trein tot stilstand. Probeer dit zelf eens.";
let tekstUitlegVijf = " Bij noodgevallen op het spoor maakt de machinist gebruik van de noodrem. De noodrem zit links van je. Probeer deze knop alvast uit, voordat je hem dalijk echt nodig hebt.";
let tekstUitlegAchteraf = " Nu je de belangrijkste knoppen begrijpt in de treincabine, kan je zelf de trein besturen. De conducteur geeft het sein om te vertrekken, wacht daar dus op!";
let k = 0;
let speed = 40;
let textResult = "";

//Functie voor het plaatsen van de uitleg
const textType = () => {
    ingesproken1.play();
    if (k < tekstUitlegEen.length) {
        k++;
        textResult = textResult + tekstUitlegEen.charAt(k);
        setTimeout(textType, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        k = 0;
        textResult = "";
        setTimeout(textTypeVooruitButton, 3500);
    }
};
const textTypeVooruitButton = () => {
    ingesproken2.play();
    if (k < tekstUitlegTwee.length) {
        k++;
        textResult = textResult + tekstUitlegTwee.charAt(k);
        setTimeout(textTypeVooruitButton, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        k = 0;
        textResult = "";
        buttonForwardTouched = true;
        buttonFlickering = false;
        intervalFlickering = setInterval(changeOpacityButtonForward, 500);
    }
};
const textTypeNeutraalButton = () => {
    ingesproken3.play();
    if (k < tekstUitlegDrie.length) {
        k++;
        textResult = textResult + tekstUitlegDrie.charAt(k);
        setTimeout(textTypeNeutraalButton, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        k = 0;
        textResult = "";
        buttonNeutralTouched = true;
        intervalFlickering = setInterval(changeOpacityButtonNeutral, 500);
    }
};
const textTypeAchteruitButton = () => {
    ingesproken4.play();
    if (k < tekstUitlegVier.length) {
        k++;
        textResult = textResult + tekstUitlegVier.charAt(k);
        setTimeout(textTypeAchteruitButton, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        k = 0;
        textResult = "";
        intervalFlickering = setInterval(changeOpacityButtonBackwards, 500);
        buttonBackwardsTouched = true;
    }
};
const textTypeNoodButton = () => {
    ingesproken6.play();
    if (k < tekstUitlegVijf.length) {
        k++;
        textResult = textResult + tekstUitlegVijf.charAt(k);
        setTimeout(textTypeNoodButton, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        k = 0;
        textResult = "";
        intervalFlickering = setInterval(changeOpacityButtonNood, 500);
        buttonNoodTouched = true;
    }
};
const textTypeAchteraf = () => {
    ingesproken5.play();
    if (k < tekstUitlegAchteraf.length) {
        k++;
        textResult = textResult + tekstUitlegAchteraf.charAt(k);
        setTimeout(textTypeAchteraf, speed);
        uitlegKnoppen.setAttribute("text", "value: " + textResult);
    } else {
        setStartLevel();
    }
};
const changeOpacityButtonForward = () => {
    if (!buttonFlickering) {
        triangleButtonForward.setAttribute("material", "opacity: 0.400");
        buttonFlickering = true;
    } else if (buttonFlickering) {
        triangleButtonForward.setAttribute("material", "opacity: 0.900");
        buttonFlickering = false;
    }
};
const changeOpacityButtonBackwards = () => {
    if (!buttonFlickering) {
        triangleButtonBackwards.setAttribute("material", "opacity: 0.400");
        buttonFlickering = true;
    } else if (buttonFlickering) {
        triangleButtonBackwards.setAttribute("material", "opacity: 0.900");
        buttonFlickering = false;
    }
};
const changeOpacityButtonNeutral = () => {
    if (!buttonFlickering) {
        circleButtonNeutral.setAttribute("material", "opacity: 0.400");
        buttonFlickering = true;
    } else if (buttonFlickering) {
        circleButtonNeutral.setAttribute("material", "opacity: 0.800");
        buttonFlickering = false;
    }
};
const changeOpacityButtonNood = () => {
    if (!buttonFlickering) {
        circleNoodButton.setAttribute("material", "opacity: 0.400");
        buttonFlickering = true;
    } else if (buttonFlickering) {
        circleNoodButton.setAttribute("material", "opacity: 0.800");
        buttonFlickering = false;
    }
};
//activeren timer, beginnen met rijden van trein.
const setTime = () => {
    timerSet = true;
    if (isRiding && isAlreadyRiding) {
        trainIsAlreadyRiding();
    } else if (isRiding) {
        trainIsRiding();
    } else if (isStopping) {
        trainIsStopping();
    } else if (!isRiding && !isStopping) {
        trainIsNeutral();
    } else if (isRiding && noodButton) {
        trainIsStopping();
    }
};
const setSecondTime = () => {
    if (isRiding && isAlreadyRiding) {
        afterNoodknopRiding();
    } else if (isStopping) {
        secondStoppingTrain();
        setInterval(checkScore, 100)
    } else if (!isRiding && !isStopping) {
        secondSetNeutral();
    }
};

//opstarten trein
const trainIsRiding = () => {
    totalSeconds++;
    setSpeedTrain("easeInSine", "50000", "14 -4 700", "false");
    timerText.setAttribute("value", totalSeconds + " km/h");
    popupSections();
};

//trein in beweging
const trainIsAlreadyRiding = () => {
    totalSeconds++;
    let positionTrain = `14 -4 ${environmentMovement.getAttribute("position").z + 40}`;
    setSpeedTrain("linear", "3000", positionTrain, "true");
    timerText.setAttribute("value", totalSeconds + " km/h");
    popupSections();
};

//afremmen trein
const trainIsStopping = () => {
    totalSeconds--;
    timerText.setAttribute("value", totalSeconds + " km/h");
    let positionTrainRemmen = `14 -4 ${environmentMovement.getAttribute("position").z + 40}`;
    setSpeedTrain("easeOutSine", "7000", positionTrainRemmen, "false");
    checkSecondsNotNull();
};

const afterNoodknopRiding = () => {
    totalSeconds++;
    timerText.setAttribute("value", totalSeconds + " km/h");
    setSecondSpeedTrain("easeInSine", "50000", "21.5 -4 700", "false");
};
const secondStoppingTrain = () => {
    totalSeconds--;
    timerText.setAttribute("value", totalSeconds + " km/h");
    let positionTrainRemmen = `21.5 -4 ${environmentMovement2.getAttribute("position").z + 40}`;
    setSecondSpeedTrain("easeOutSine", "7000", positionTrainRemmen, "false");
    checkSecondsNotNull();
};
const secondSetNeutral = () => {
    increaseSpeed = true;
    let positionTrain = `14 -4 ${environmentMovement2.getAttribute("position").z + 40}`;
    timerText.setAttribute("value", totalSeconds + " km/h");
    setSpeedTrain("linear", "3000", positionTrain, "true");
    popupSections();
};
const checkSecondsNotNull = () => {
    if (totalSeconds === 0 && checkSecond) {
        stopSetTimer(setSnelheid);
        pauseSecondEnvironmentMovement();
    } else if (totalSeconds === 0) {
        stopSetTimer(setSnelheid);
        pauseEnvironmentMovement();
    }
};

//trein in zijn neutraal
const trainIsNeutral = () => {
    increaseSpeed = true;
    let positionTrain = `14 -4 ${environmentMovement.getAttribute("position").z + 40}`;
    timerText.setAttribute("value", totalSeconds + " km/h");
    setSpeedTrain("linear", "3000", positionTrain, "true");
};

//animatie voor treinSpeed meegeven
const setSpeedTrain = (easing, duration, position, loop) => {
    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property: position; easing: " + easing + "; dur: " + duration + "; to:" + position + "; loop: " + loop;
    environmentMovement.setAttribute("animation", animation_attribute.value);
};
const setSecondSpeedTrain = (easing, duration, position, loop) => {
    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property: position; easing: " + easing + "; dur: " + duration + "; to:" + position + "; loop: " + loop;
    environmentMovement2.setAttribute("animation", animation_attribute.value);
};

//movement aangeven van de hendel, aanmaken van de animatie
const changePosition = (position) => {
    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property:rotation; easing: linear; dur: 1000; to:" + position;
    hendel.setAttribute("animation", animation_attribute.value);
};
const changePostionCamera = (position) => {
    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property:rotation; easing: linear; dur: 2000; to:" + position;
    rig.setAttribute("animation", animation_attribute.value);
};

buttonForward.onclick = () => {
    if (buttonForwardTouched) {
        clearInterval(intervalFlickering);
        triangleButtonForward.setAttribute("material", "opacity: 0.000");
        camera.setAttribute("look-controls", "enabled");
        buttonForwardTouched = false;
        setTimeout(textTypeNeutraalButton, 2000);
    }
};

//hendel movement incl functies aanroepen
buttonForward.onmouseenter = (event) => {
    if (checkStart && increaseSpeed && opdrachtVoltooid) {
        changePosition("-50 0 0");
        isRiding = true;
        increaseSpeed = false;
        setSnelheid = setInterval(setTime, 500);
        opdrachtVoltooid = false;
    } else if (checkStart && increaseSpeed) {
        changePosition("-50 0 0");
        isRiding = true;
        increaseSpeed = false;
        setSnelheid = setInterval(setTime, 500);
    } else if (checkSecond && increaseSpeed) {
        isRiding = true;
        isAlreadyRiding = true;
        setSnelheid = setInterval(setSecondTime, 500);
        changePosition("-50 0 0");
    }
};
buttonBackwards.onclick = () => {
    if (buttonBackwardsTouched) {
        triangleButtonBackwards.setAttribute("material", "opacity: 0.000");
        clearInterval(intervalFlickering);
        buttonBackwardsTouched = false;
        textTypeNoodButton();
    }
};
buttonBackwards.onmouseenter = (event) => {
    if (checkStart && opdrachtVoltooid) {
        changePosition("50 0 0");
        opdracht.setAttribute("material", "color: red");
        opdracht.setAttribute("text", "value: Deze knop is niet juist! Probeer het nog een keer!");
        setTimeout(setTextOpdracht1Back, 2000);
        opdrachtVoltooid = true;
    } else if (checkStart) {
        isStopping = true;
        changePosition("50 0 0");
        setSnelheid = setInterval(setTime, 300);
    } else if (checkSecond) {
        isStopping = true;
        changePosition("50 0 0");
        setSnelheid = setInterval(setTime, 500);
    }
};
const setTextOpdracht1Back = () => {
    opdracht.setAttribute("text", "value: Opdracht 1: Breng de trein in beweging!");
    opdracht.setAttribute("material", "color: white");
};
buttonNeutral.onclick = () => {
    if (buttonNeutralTouched) {
        circleButtonNeutral.setAttribute("material", "opacity: 0.000");
        buttonNeutralTouched = false;
        clearInterval(intervalFlickering);
        setTimeout(textTypeAchteruitButton, 1500);
    }
};
buttonNeutral.onmouseenter = (event) => {
    if (checkStart) {
        isRiding = false;
        changePosition("0 0 0");
        isStopping = false;
        isAlreadyRiding = true;
        increaseSpeed = true;
        clearInterval(setSnelheid);
    } else if (checkSecond) {
        isRiding = false;
        isStopping = false;
        clearInterval(setSnelheid);
        changePosition("0 0 0");
    }
};

//acties voor starten en pauseren environment
const resumeEnvironmentMovement = () => {
    environmentMovement.play();
    setSnelheid = setInterval(setTime, 500);
};
const pauseEnvironmentMovement = () => {
    environmentMovement.pause();
};
const pauseSecondEnvironmentMovement = () => {
    environmentMovement2.pause();
    stopSetTimer(setSnelheid);
};
const resumeSecondEnvironmentMovement = () => {
    environmentMovement2.play();
    setSnelheid = setInterval(setTime, 500);
};
const stopSetTimer = (setSnelheid) => clearInterval(setSnelheid);

const stopSetObstakel = (obstakelTouched) => clearInterval(obstakelTouched);

//meegeven popups secties tijdens het rijden van de trein
const popupSections = () => {
    if (totalSeconds === 35) {
        checkStart = false;
        setUitlegTwee();
        stopSetTimer(setSnelheid);
    } else if (totalSeconds === 80) {
        cowStart = true;
        stopSetTimer(setSnelheid);
        setTimeout(setUitlegVier, 5000);
        opdracht.setAttribute("text", "value: Goed gedaan! Opdracht 3: Stop optijd voor de koe, maak hiervoor gebruik van de noodrem");
    } else if (totalSeconds === 120) {
        changePosition("0 0 0");
        console.log("Rijd niet te hard");
        totalSeconds = 120;
        stopSetTimer(setSnelheid);
    }
};

//set uitleg verschillende levels, scene 1
const setUitlegTwee = () => {
    environmentMovement.pause();
    bordSnelheid.setAttribute("visible", "true");
    uitlegLevelTwee.setAttribute("visible", "true");
    verderButtonTwee.setAttribute("visible", "true");
    verderButtonTwee.className = "clickable";
    vraagteken.remove();
    uitleg2.play();
    vraagtekenTwee.setAttribute("visible", "true");
    vraagtekenTwee.className = 'clickable';
};

const setUitlegDrie = () => {
    uitlegLevelTwee.remove();
    verderButtonTwee.remove();
    verderButtonDrie.className = "clickable";
    uitleg1.play();
    uitleg2.pause();
    uitlegLevelTweeTwee.setAttribute("visible", "true");
    verderButtonDrie.setAttribute("visible", "true");

};
const removeLevelDrie = () => {
    uitlegLevelTweeTwee.remove();
    verderButtonDrie.remove();
    bordSnelheid.remove();
    checkStart = true;

};
verderButtonTwee.onclick = (event) => {
    setUitlegDrie();
};
verderButtonDrie.onclick = (event) => {
    resumeEnvironmentMovement();
    removeLevelDrie();
    uitleg1.pause();
    checkStart = true;
    opdracht.setAttribute("material", "color: white");
    opdracht.setAttribute("text", "value: Opdracht 2: Bereik een snelheid van 80 km/h, hierna moet de trein naar zijn neutraal worden gezet");
};
const setTrainElements = () => {
    checkStart = false;
    hendel.setAttribute("visible", "true");
    camera.setAttribute("look-controls", "enabled");
    button_top.setAttribute("visible", "true");
    buttonForward.setAttribute("visible", "true");
    buttonNeutral.setAttribute("visible", "true");
    buttonBackwards.setAttribute("visible", "true");
    emergency.setAttribute("visible", "true");
    cabine.setAttribute("visible", "true");
    uitlegKnoppen.setAttribute("visible", "true");
    vraagteken.setAttribute("visible", "true");
    vraagtekenTwee.setAttribute("visible", "false");
    vraagtekenDrie.setAttribute("visible", "false");
    vraagtekenVier.setAttribute("visible", "false");
    rig.setAttribute("position", "0 6 6");
    rig.setAttribute("rotation", "0 0 0");
    textType();
};
const setStartLevel = () => {
    uitlegTwee.setAttribute("visible", "true");
    startButton.setAttribute("visible", "true");
    startButton.className = 'clickable';
    vraagteken.className = 'clickable';
};

const setUitlegEen = () => {
    uitlegEen.setAttribute("visible", "false");
    verderButton.setAttribute("visible", "false");

};
const removeUitlegTwee = () => {
    uitlegTwee.remove();
    startButton.remove();
    checkStart = true;
    increaseSpeed = true;
    opdrachtVoltooid = true;
    opdracht.setAttribute("text", "value: Opdracht 1: Breng de trein in beweging!");
};

const setUitlegVier = () => {
    checkStart = false;
    environmentMovement.pause();
    clearInterval(setSnelheid);
    verderButtonVier.className = "clickable";
    uitlegLevelDrie.setAttribute("visible", "true");
    verderButtonVier.setAttribute("visible", "true");
    console.log(environmentMovement.getAttribute("position"));
    vraagtekenTwee.remove();
    uitleg3.play();
    vraagtekenDrie.setAttribute("visible", "true");
    vraagtekenDrie.className = 'clickable';
};
const removeUitlegVier = () => {
    checkStart = true;
    setInterval(setTime, 500);
    uitlegLevelDrie.remove();
    verderButtonVier.remove();
    verderButtonVijf.className = "clickable";
};
const removeVerderVijf = () => {
    verderButtonVijf.remove();
    environmentMovement.remove();
    failedObstakel.remove();
    succesObstakel.remove();
    checkStart = false;
    checkSecond = true;
    increaseSpeed = true;
    timerText.setAttribute("value", 80 + " km/h");
    vraagtekenDrie.remove();

};
const SetUitleglevelVier = () => {
    pauseSecondEnvironmentMovement();
    uitlegLevelVier.setAttribute("visible", "true");
    verderButtonZes.setAttribute("visible", "true");
    uitleg4.play();
    verderButtonZes.className = "clickable";
    stopSetTimer(setSnelheid);
    checkSecond = false;
};

// Setting Button action verderButtonZes
verderButtonZes.onclick = (event) => {
    resumeSecondEnvironmentMovement();
    setTimeout(removeVerderZes, 2000);
    uitleg4.pause();
};
//Removing verderButtonZes and moving train
const removeVerderZes = (event) => {
    setSnelheid = setInterval(setSecondTime, 500);
    verderButtonZes.remove();
    uitlegLevelVier.remove();
    checkSecond = true;
    setTimeout(showSBord, 10000);
    environmentMovement2.play();


};
verderButton.onclick = (event) => {
    setUitlegEen();
    setTrainElements();
};
startButton.onclick = (event) => {
    removeUitlegTwee();
    geluidStart.play();
    uitlegKnoppen.setAttribute("text", "value: ");
    setTimeout(laatHorenGeluidFluiten, 11000);
};
const laatHorenGeluidFluiten = () => {
    geluidFluit.play();
};
verderButtonVier.onclick = (event) => {
    removeUitlegVier();
    resumeEnvironmentMovement();
    koeMovement();
    uitleg3.pause();
    checkStart = true;
    obstakelTouched = setInterval(checkPositionTrainAndCow, 250);
};
// Start level 4
const setLevel4 = () => {
    environmentMovement2.setAttribute("obj-model", "obj: #station2-obj; mtl: #station2-mtl");
    totalSeconds = 0;
    opdracht.setAttribute("text", "value:Opdracht 4: Breng te trein tot stilstand doormiddel van de hendel");
    vraagtekenVier.setAttribute("visible", "true");
    vraagtekenVier.className = 'clickable';

};

verderButtonVijf.onclick = (event) => {
    removeVerderVijf();
    setLevel4();
    afterNoodknopRiding();
    setTimeout(SetUitleglevelVier, 5000);
};

//Setting Button action verderButtonZes
verderButtonZes.onclick = (event) => {
    removeVerderZes();
};

//Noodrem actie
const noodremAction = () => {
    let positionTrain = `14 -4 ${environmentMovement.getAttribute("position").z + 100}`;
    setSpeedTrain("easeOutSine", "7000", positionTrain, "false");
    stopSetTimer(setSnelheid);
    setSnelheid = setInterval(decreaseSpeed, 50);
};
const decreaseSpeed = () => {
    totalSeconds--;
    timerText.setAttribute("value", totalSeconds + " km/h");
    if (totalSeconds === 0 && cowStart) {
        obstalkelKoe.pause();
        cowFailed();
        pauseEnvironmentMovement();
        stopSetTimer(setSnelheid);
    } else if (totalSeconds === 0 && cowStart === false) {
        pauseEnvironmentMovement();
        stopSetTimer(setSnelheid);
    }
};
button_top.onclick = (event) => {
    if (buttonNoodTouched) {
        circleNoodButton.setAttribute("material", "opacity: 0.000");
        clearInterval(intervalFlickering);
        buttonNoodTouched = false;
        setTimeout(textTypeAchteraf, 2000);
    }
};
button_top.onmouseenter = (event) => {
    if (checkStart && timerSet) {
        noodButton = true;
        buttonAction("-3.143 2.926 4.818");
        stopSetTimer(setSnelheid);
        setSnelheid = setInterval(decreaseSpeed, 60);
    }
};
button_top.onmouseleave = (event) => {
    buttonAction("-3.143 2.966 4.818");
};
const buttonAction = (position) => {
    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property: position; easing: linear; dur: 300; to:" + position;
    button_top.setAttribute("animation", animation_attribute.value);
};


//Noodknop actie, obstakel komt op
const koeMovement = () => {
    obstalkelKoe.setAttribute("visible", "true");

    let positionCow = `0 5 0`;

    const animation_attribute = document.createAttribute("animation");
    animation_attribute.value = "property: position; easing: linear; dur: 10000; to: " + positionCow + "; loop: false";
    obstalkelKoe.setAttribute("animation", animation_attribute.value);
};

const checkPositionTrainAndCow = () => {
    let positionCow = obstalkelKoe.getAttribute("position").z;
    if (positionCow >= -2.5) {
        obstalkelKoe.pause();
        cowSuccesed();
        stopSetTimer(setSnelheid);
        stopSetObstakel(obstakelTouched);
    }
};
const cowFailed = () => {
    verderButtonVijf.setAttribute("visible", "true");
    obstalkelKoe.remove();
    environmentMovement.pause();
    stopSetObstakel(obstakelTouched);
    stopSetTimer(setSnelheid);
    failedObstakel.setAttribute("visible", "true");

    setInterval(getPositionOfTrainAfterStopping, 500)

    if (totalSeconds = 0) {
        stopSetTimer(setSnelheid);
    }
};
const cowSuccesed = () => {
    obstalkelKoe.remove();
    succesObstakel.setAttribute("visible", "true");
    verderButtonVijf.setAttribute("visible", "true");

    setInterval(getPositionOfTrainAfterStopping, 500);

    stopSetObstakel(obstakelTouched);
    stopSetTimer(setSnelheid);
    // stopSetTimer(setSnelheid);
    if (totalSeconds === 0) {
        stopSetTimer(setSnelheid);
    }
};
const showSBord = () => {
    Sbord.setAttribute("visible", "true");
    uitlegSBord.setAttribute("visible", "true");
    verderSBord.setAttribute("visible", "true");
    uitleg5.play();
    verderSBord.className = 'clickable';
    checkSecond = false;
    pauseSecondEnvironmentMovement();
    stopSetTimer(setSnelheid);
    clearInterval(setSnelheid);
};
verderSBord.onclick = (event) => {
    Sbord.remove();
    uitlegSBord.remove();
    verderSBord.remove();
    geluidAankomst.play();
    resumeSecondEnvironmentMovement();
    checkSecond = true;
    uitleg5.pause();
};

const getPositionOfTrainAfterStopping = () => {
    let positionTrain = environmentMovement2.getAttribute("position").z;
    console.log(positionTrain);
};
const verderButtonLaatste = () => {
    verderLaatste.setAttribute("visible", "true");
    verderLaatste.className = 'clickable';
    stopSetTimer(setSnelheid);
};
//Level4
const checkScore = () => {
    let positionTrain = environmentMovement2.getAttribute("position").z;
    if (totalSeconds === 0 && positionTrain <= 300) {
        console.log("1 STERREN!!!!!");
        eenSter.setAttribute("visible", "true");
        pauseSecondEnvironmentMovement();
        verderButtonLaatste();
        stopSetTimer(setSnelheid);
        totalSeconds = 0;
    } else if (totalSeconds === 0 && positionTrain <= 540) {
        console.log("2 STERREN!!!!!");
        tweeSter.setAttribute("visible", "true");
        verderButtonLaatste();
        pauseSecondEnvironmentMovement();
        stopSetTimer(setSnelheid);
        totalSeconds = 0;
    } else if (totalSeconds === 0 && positionTrain <= 620) {
        console.log("3 STERREN!!!!!");
        drieSter.setAttribute("visible", "true");
        verderButtonLaatste();
        pauseSecondEnvironmentMovement();
        stopSetTimer(setSnelheid);
        totalSeconds = 0;
    } else if (totalSeconds === 0 && positionTrain > 620) {
        console.log("Helaas!!!!!");
        helaas.setAttribute("visible", "true");
        verderButtonLaatste();
        totalSeconds = 0;
    } else if (positionTrain === 700) {
        console.log("Helaas!!!!!");
        helaas.setAttribute("visible", "true");
        pauseSecondEnvironmentMovement();
        verderButtonLaatste();
        totalSeconds = 0;
        stopSetTimer(setSnelheid);
    }
};
verderLaatste.onclick = (event) => {
    eindeUitleg.setAttribute("visible", "true");
    helaas.remove();
    drieSter.remove();
    tweeSter.remove();
    eenSter.remove();
    verderLaatste.remove();
};
// Code API
const setTextToSpeech = (messageGiven) => {
    responsiveVoice.speak(messageGiven, "Dutch Male");
};

// Vraagteken level 1
vraagteken.addEventListener('click', () => {
    setTextToSpeech("Bij level één moet je de trein vooruit rijden door middel van tractie. Vergeet hem niet terug in zijn neutraal te zetten!");
});

// Vraagteken level 2
vraagtekenTwee.addEventListener('click', () => {
    setTextToSpeech("Bij level twee moet je de constante snelheid behouden! Gebruik hiervoor de knoppen");
});

// Vraagteken level 3
vraagtekenDrie.addEventListener('click', () => {
    setTextToSpeech("Bij level drie moet je op tijd op de noodknop klikken tijdens een noodsituatie");
});

// Vraagteken level 4
vraagtekenVier.addEventListener('mouseleave', () => {
    setTextToSpeech("Zorg dat je op tijd afremt bij het station om de reizigers veilig bij het perron te brengen");
});
